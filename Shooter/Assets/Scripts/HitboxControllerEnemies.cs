using System;
using System.Collections;
using System.Collections.Generic;
using Interfaces;
using UnityEngine;

public class HitboxControllerEnemies : MonoBehaviour, IDamageable
{
    public event Action<int> OnDamage;
    public void Damage(int amount)
    {
       
        Debug.Log($"{gameObject.tag} impactado por un valor de {amount} puntos de daño.");
        OnDamage?.Invoke(amount);

    }
    
}
