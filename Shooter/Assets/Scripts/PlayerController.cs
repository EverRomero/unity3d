using System;
using System.Collections;
using System.Collections.Generic;
using Interfaces;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;

public class PlayerController : MonoBehaviour
{
   // STATE MACHINE Y MOVIMIENTO PLAYER

    [SerializeField]
    private InputActionAsset inputAsset;
    private InputActionAsset input;
    private InputAction movementPlayerAction;

    private Rigidbody playerRigidbody;

    private Transform cameraTransform;

    [SerializeField]
    private float sensitivity = 1f;


    Vector3 camRotation;

    [SerializeField]
    private LayerMask shootMask;

    [SerializeField]
    GameEvent hpText;


    /* [SerializeField]
      private HitboxInfo hitboxInfo;*/

    [Header("Character Values")]
    [SerializeField]
    private float moveSpeed = 6f;

    private Vector3 movement = Vector3.zero;

    [SerializeField] int hpMax = 5;

    private int currentHp;


    // Guns

    private bool gun = true;
    private bool gun2 = false;
    

    private bool gun2Ready = true;


    // Camera

    [SerializeField] GameObject mainCamera;
    [SerializeField] GameObject camera2;

    private void Awake()
    {

        Assert.IsNotNull(inputAsset);

        input = Instantiate(inputAsset);
        input.FindActionMap("Player").FindAction("Shoot").performed += Shoot;
        input.FindActionMap("Player").FindAction("Gun").performed += EquipGun;
        input.FindActionMap("Player").FindAction("Gun2").performed += EquipGun2;
        input.FindActionMap("Player").FindAction("ChangeCamera").performed += ChangeCamera;
        input.FindActionMap("Player").FindAction("Jump").performed += Jump;
        input.FindActionMap("Player").Enable();

        playerRigidbody = GetComponent<Rigidbody>();
        cameraTransform = Camera.main.transform;

    }
    

    private void Start()
    {
        currentHp = hpMax;

        if (this.TryGetComponent<IDamageable>(out IDamageable damageable))
        {
            damageable.OnDamage += Hit;

        }

        InitState(SwitchMachineStates.IDLE);
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {

        UpdateState();
        transform.Rotate(Vector3.up * sensitivity * Input.GetAxis("Mouse X"));

        camRotation.x -= Input.GetAxis("Mouse Y") * sensitivity;
        camRotation.x = Mathf.Clamp(camRotation.x, -30, 45);

        cameraTransform.transform.localEulerAngles = camRotation;
        
        
    }

    private void FixedUpdate()
    {
       // Debug.Log("Velocidad y:"+ playerRigidbody.velocity.y);
       playerRigidbody.velocity = movement.normalized * moveSpeed + Vector3.up * playerRigidbody.velocity.y;
    }
    // --------------------------------------------------   STATE MACHINE -------------------------------------------------------------//

    private enum SwitchMachineStates {NONE, IDLE, WALK, SHOOT, SHOOT2};
    private SwitchMachineStates currentState;

    private void ChangeState(SwitchMachineStates newState)
    {
       
        if (newState == currentState)
            return;

        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        this.currentState = currentState;
        switch (this.currentState)
        {
            case SwitchMachineStates.IDLE:

                playerRigidbody.velocity = Vector3.zero;

                break;

            case SwitchMachineStates.WALK:
                
                break;

            case SwitchMachineStates.SHOOT:

                TriggerShoot();
                EndShoot();
                break;

            case SwitchMachineStates.SHOOT2:

                StartCoroutine(CouldownShoot2());
                EndShoot();
                break;

            default:
                break;
        }
    }


    private void ExitState()
    {
        switch (currentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.WALK:

                break;

            case SwitchMachineStates.SHOOT:
                break;

            case SwitchMachineStates.SHOOT2:
                break;

            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (currentState)
        {
            case SwitchMachineStates.IDLE:
                if (input.FindActionMap("Player").FindAction("Move").ReadValue<Vector2>() != Vector2.zero);
                    ChangeState(SwitchMachineStates.WALK);
                break;
            
            case SwitchMachineStates.WALK:

                Vector2 inputMove = input.FindActionMap("Player").FindAction("Move").ReadValue<Vector2>();

                movement = inputMove.y * transform.forward + inputMove.x * transform.right;
                
                if(movement == Vector3.zero)
                    ChangeState(SwitchMachineStates.IDLE);
                
                break;

            case SwitchMachineStates.SHOOT:
                break;

            case SwitchMachineStates.SHOOT2:
                break;

            default:
                break;
        }

    }
    
    private void Shoot(InputAction.CallbackContext actionContext)
    {

        if (gun == true)
        {

            ChangeState(SwitchMachineStates.SHOOT);

        }else if (gun2 == true) {

            ChangeState(SwitchMachineStates.SHOOT2);

        }

        
    }

    private void TriggerShoot()
    {
        RaycastHit hit;
        //Vector3 accuracy = new Vector3(UnityEngine.Random.Range(-0.1f, 0.1f), UnityEngine.Random.Range(-0.1f, 0.1f), 0);
        if (Physics.Raycast(cameraTransform.transform.position, cameraTransform.transform.forward/*+accuracy*/, out hit, 30f, shootMask))
        {
            Debug.Log($"He tocat {hit.collider.gameObject.tag} a la posici {hit.point} amb normal {hit.normal}");
            Debug.DrawLine(cameraTransform.transform.position, hit.point, Color.green, 2f);

            if (hit.collider.TryGetComponent<IDamageable>(out IDamageable target)) 
            {
                if (hit.collider.gameObject.CompareTag("Head"))
                {
                    target.Damage(10);
                    
                }else if (hit.collider.gameObject.CompareTag("Chest"))
                {
                    target.Damage(5);
                }
                else
                {
                    target.Damage(1);
                }
                        
                        
            }

            if (hit.collider.TryGetComponent<IPushable>(out IPushable pushable))
            {
                pushable.Push(mainCamera.transform.forward, 500);
            }
                
            
        }
        else
        {
            Debug.Log("NO toco con ningun collider y layer permitida");
        }
    }

    private void TriggerShoot2()
    {

        if (gun2Ready)
        {

            for (int i = 0; i < 3; i++)
            {
                RaycastHit hit;
                if (Physics.Raycast(cameraTransform.transform.position, cameraTransform.transform.forward, out hit, 30f, shootMask))
                {
                    Debug.Log($"He tocat {hit.collider.gameObject.tag} a la posici {hit.point} amb normal {hit.normal}");
                    Debug.DrawLine(cameraTransform.transform.position, hit.point, Color.green, 2f);

                    if (hit.collider.TryGetComponent<IDamageable>(out IDamageable target)) 
                    {
                        if (hit.collider.gameObject.CompareTag("Head"))
                        {
                            target.Damage(10);
                    
                        }else if (hit.collider.gameObject.CompareTag("Chest"))
                        {
                            target.Damage(5);
                        }
                        else
                        {
                            target.Damage(1);
                        }
                        
                        
                    }
                    
                    if (hit.collider.TryGetComponent<IPushable>(out IPushable pushable))
                    {
                        pushable.Push(mainCamera.transform.forward, 500);
                    }

                    Debug.Log("SE HA DISPARADO CORRECTAMENTE");

                }else
                {
                    Debug.Log("SE HA DISPARADO CORRECTAMENTE PERO NO toco con ningun collider y layer permitida");
                }
            }
        }
        else
        {

            Debug.Log("EL DISPARO SE ESTA CARGANDO...");

        }
    }
    
    IEnumerator CouldownShoot2()
    {
        
        TriggerShoot2();
        gun2Ready = false;
        yield return new WaitForSeconds(4);
        gun2Ready = true;

    }


    private void EquipGun(InputAction.CallbackContext actionContext)
    {

        Debug.Log("SE EQUIPO LA PISTOLA");

        gun = true;
        gun2 = false;

    }

    private void EquipGun2(InputAction.CallbackContext actionContext)
    {
        Debug.Log("SE EQUIPO LA FAMAS");

        gun = false;
        gun2 = true;

    }

    private void ChangeCamera(InputAction.CallbackContext actionContext)
    {

        if(mainCamera.activeSelf == true)
        {

            mainCamera.SetActive(false);
            camera2.SetActive(true);

        }else if(camera2.activeSelf == true)
        {
            camera2.SetActive(false);
            mainCamera.SetActive(true);
           
        }

    }

    private void Jump(InputAction.CallbackContext actionContext)
    {

        playerRigidbody.AddForce(Vector3.up * 10, ForceMode.Impulse);


    }

    private void Hit(int damage)
    {
        currentHp -= damage;
        GameManager.Instance.getSetCurrentHp = currentHp;

        Debug.Log("Player: Hp actual: " + currentHp);
        
        if (currentHp <= 0)
            Destroy(this.gameObject);

        hpText.Raise();
    }


    public void EndShoot()
    {
        ChangeState(SwitchMachineStates.IDLE);
    }
    

    public void endAnimation()
    {

        EndShoot();
    }

    private void OnDestroy()
    {

        input.FindActionMap("Player").FindAction("Shoot").performed -= Shoot;
        input.FindActionMap("Player").FindAction("Gun").performed -= EquipGun;
        input.FindActionMap("Player").FindAction("Gun2").performed -= EquipGun2;
        input.FindActionMap("Player").FindAction("ChangeCamera").performed -= ChangeCamera;
        input.FindActionMap("Player").FindAction("Jump").performed += Jump;
        input.FindActionMap("Player").Disable();

        UnityEditor.EditorApplication.isPlaying = false;

    }

    
}


