using System;
using System.Collections;
using System.Collections.Generic;
using Interfaces;
using UnityEngine;

public class HitboxController : MonoBehaviour, IDamageable, IPushable
{
    public event Action<int> OnDamage;
    public void Damage(int amount)
    {
       
        Debug.Log($"{gameObject.tag} impactado por un valor de {amount} puntos de daño.");
        OnDamage?.Invoke(amount);

    }
    
    public void Push(Vector3 direction, float impulse)
    {
        GetComponent<Rigidbody>().AddForce(direction * impulse, ForceMode.Impulse);
    }
    
}
