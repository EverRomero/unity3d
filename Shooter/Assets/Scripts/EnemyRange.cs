using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class EnemyRange : MonoBehaviour
{
   [SerializeField] private SOEnemy statsEnemy;
    private bool waitingForNextPath;
    Vector3 nextDestination;
    
    [SerializeField]
    private LayerMask shootMask;

    private float hpMax;
    private float hpActual;
    private float moveSpeed;
    private float damage;
    private float rangeAttack;
    private float rangeDetection;
    private Color colorOfEnemy;
    private bool canAttack = true;

    [SerializeField] private LayerMask layerMask;

    private bool playerTraked;
    private Vector3 originPosition;

    private Collider[] playerCollider;

    private UnityEngine.AI.NavMeshAgent agent;

    protected enum SwitchMachineStates
    {
        NONE,
        IDLE,
        WALK,
        ATTACK,
        RECIVEHIT,
        DIE
    };

    protected SwitchMachineStates currentState;
    protected SwitchMachineStates lastState;

    [SerializeField] private Transform shootTarget;   
    

    private bool inAttack = false;

    void Start()
    {

        this.moveSpeed = statsEnemy.moveSpeed;
        this.hpActual = statsEnemy.maxHP;
        this.damage = statsEnemy.damage;
        this.rangeAttack = statsEnemy.rangeAttack;
        this.rangeDetection = statsEnemy.rangeDetection;
        
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();

        waitingForNextPath = false;

        if (this.TryGetComponent<IDamageable>(out IDamageable damageable))
        {
            damageable.OnDamage += OnHit;

        }
        
        originPosition = transform.position;
        InitState(SwitchMachineStates.IDLE);
    }
    
    void Update()
    {
        playerCollider = Physics.OverlapSphere(transform.position, rangeDetection, layerMask);
        UpdateState();
        
        if (playerCollider.Length > 0)
        {
            shootTarget = playerCollider[0].gameObject.transform;
            TrackedPlayer(playerCollider[0]);
        }
            
  
        if (playerCollider.Length <= 0 && playerTraked)
            StartCoroutine(StayOnSite());
        else if (agent.remainingDistance < 1.2f && !waitingForNextPath && playerCollider.Length <= 0)
            StartCoroutine(CooldownToNextPoint(5f));
    }

    IEnumerator StayOnSite()
    {
        agent.speed = 0;
        waitingForNextPath = true;
        yield return new WaitForSeconds(1.5f);
        agent.SetDestination(originPosition);
    }

    void TrackedPlayer(Collider playerCol)
    {
        StopCoroutine(StayOnSite());
        agent.SetDestination(playerCol.gameObject.transform.position);
        float distance = Vector2.Distance(agent.destination, this.transform.position);
        if (distance < rangeAttack)
        {
            agent.speed = 0;
            if (canAttack)
            {
                ShootToPlayer();
                ChangeState(SwitchMachineStates.ATTACK);
            }
        }
        else
            agent.speed = moveSpeed;
    }

    private void ShootToPlayer()
    {
        canAttack = false;

        Vector3 accuracy = new Vector3(Random.Range(-10f, 10f), Random.Range(-10f, 10f), 0);
        
        RaycastHit hit;
        
        if (Physics.Raycast(this.transform.position, (shootTarget.position-this.transform.position)+accuracy, out hit, 15f, shootMask))
        {
            Debug.Log($"{this.gameObject.name} He tocat {hit.collider.gameObject} a la posici {hit.point} amb normal {hit.normal}");
            Debug.DrawLine(this.transform.position, hit.point, Color.green, 2f);

            if (hit.collider.TryGetComponent<IDamageable>(out IDamageable target))
            {
                target.Damage(1);
            }

        }
        else
        {
            Debug.Log(this.gameObject.name+" - NO toco con ningun collider y layer permitida");
        }

        StartCoroutine(ShootCoolDown(1.5f));
    }

    IEnumerator ShootCoolDown(float time)
    {
        yield return new WaitForSeconds(time);
        canAttack = true;
        Debug.Log(this.gameObject.name+" SHOOOOOOOT");
    }

    IEnumerator CooldownToNextPoint(float time)
    {
        agent.speed = 0;
        waitingForNextPath = true;
        yield return new WaitForSeconds(time);
        GotoNextPoint();
    }


    void GotoNextPoint()
    {
        float random = Random.Range(4, 15);
        switch (Random.Range(0, 4))
        {
            case 0:
                nextDestination = new Vector3(transform.position.x + random, transform.position.y,
                    transform.position.z);
                agent.SetDestination(nextDestination);
                break;
            case 1:
                nextDestination = new Vector3(transform.position.x - random, transform.position.y,
                    transform.position.z);
                agent.SetDestination(nextDestination);
                break;
            case 2:
                nextDestination =
                    new Vector3(transform.position.x, transform.position.y, transform.position.z + random);
                agent.SetDestination(nextDestination);
                break;
            case 3:
                nextDestination = new Vector3(transform.position.x, transform.position.y,
                    (transform.position.z - random));
                agent.SetDestination(nextDestination);
                break;
        }

        agent.speed = moveSpeed;
        waitingForNextPath = false;
    }


    public void OnHit(int damage)
    {
        hpActual -= damage;
        
        Debug.Log(this.gameObject.name+" Hp actual: "+hpActual);
        
        if (hpActual > 0)
            ChangeState(SwitchMachineStates.RECIVEHIT);
        else
            ChangeState(SwitchMachineStates.DIE);
            
    }

    // ********************** MACHINE STATS ********************

    #region SwitchMachineStates

    private void ChangeState(SwitchMachineStates newState)
    {
        
        
        if (newState == currentState)
            return;

        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        this.currentState = currentState;
        switch (this.currentState)
        {
            case SwitchMachineStates.IDLE:

                agent.speed = 0;
                StartCoroutine(CooldownToNextPoint(5f));

                break;


            case SwitchMachineStates.WALK:
                break;


            case SwitchMachineStates.ATTACK:
                agent.speed = 0;
                break;

            case SwitchMachineStates.RECIVEHIT:
                agent.speed = 0;
                StartCoroutine(StunTime());
                break;

            case SwitchMachineStates.DIE:
                Destroy(gameObject);
                break;

            default:
                break;
        }
    }

    IEnumerator StunTime()
    {
        yield return new WaitForSeconds(1f);
        ChangeState(lastState);
    }

    private void ExitState()
    {
        switch (currentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.WALK:

                break;

            case SwitchMachineStates.ATTACK:
                inAttack = false;
                break;

            case SwitchMachineStates.RECIVEHIT:
                break;

            default:
                break;
        }
    }

    private void UpdateState()
    {
        //Debug.Log("ESTADO ENEMIGO: "+currentState);
        
        switch (currentState)
        {
            case SwitchMachineStates.IDLE:

               // Debug.Log("Velocidad agente: "+agent.speed);
                
                if (agent.speed != 0)
                    ChangeState(SwitchMachineStates.WALK);

                lastState = currentState;
                break;

            case SwitchMachineStates.WALK:

                if (agent.speed == 0)
                    ChangeState(SwitchMachineStates.IDLE);

                lastState = currentState;
                break;

            case SwitchMachineStates.ATTACK:
                if (playerCollider.Length <= 0)
                    ChangeState(SwitchMachineStates.IDLE);
                
                break;
            case SwitchMachineStates.RECIVEHIT:
                break;
            case SwitchMachineStates.DIE:
                break;

            default:
                break;
        }
    }

    #endregion
    
}