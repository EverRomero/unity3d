using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace m17
{
    public class RagdollController : MonoBehaviour
    {
        private int hp = 50;

        private Rigidbody[] m_Bones;
        private Animator m_Animator;

        private void Awake()
        {
            m_Animator = GetComponent<Animator>();
            m_Bones = GetComponentsInChildren<Rigidbody>();
            Activate(false);

            foreach (Rigidbody bone in m_Bones)
                if(bone.TryGetComponent<IDamageable>(out IDamageable damageable))
                    damageable.OnDamage += ReceiveDamage;
        }

        public void Activate(bool state)
        {
            foreach (Rigidbody bone in m_Bones)
                bone.isKinematic = !state;
            m_Animator.enabled = !state;
        }

        private void ReceiveDamage(int damage)
        {
            hp -= damage;
            Debug.Log("RAGDOLL CURRENT HP: "+hp);
            if(hp <= 0)
                Die();
        }

        private void Die()
        {
            foreach (Rigidbody bone in m_Bones)
                if (bone.TryGetComponent<IDamageable>(out IDamageable damageable))
                    damageable.OnDamage -= ReceiveDamage;

            Activate(true);
        }
    }
}