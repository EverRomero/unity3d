using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Interfaces
{
    public interface IPushable
    {
       
        void Push(Vector3 direction, float impulse);
        
    }
}